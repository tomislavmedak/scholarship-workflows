To convert my *Digital scholarship workflows* document to a printable PDF, use the universal document converter Pandoc. If you don't have Pandoc, here you'll find [instructions how to install it](https://pandoc.org/installing.html). You will also need to install pandoc-citeproc filter to render bibliographic references.

Now clone or download the project directory and in your terminal do (note: don't forget to replace directory path to the path where you have locally cloned or extracted the project):

```
cd /path/to/scholarship-workflows/
pandoc Digital_scholarship_workflows.md --csl "harvard.csl" --bibliography "workflows.bib" --table-of-contents --toc-depth=4 --pdf-engine=xelatex -V colorlinks -V toccolor=blue -H head.tex -s -o Digital_scholarship_workflows.pdf
```

this will create a PDF file of my workflows document in the same directory where you have downloaded the project directory.
